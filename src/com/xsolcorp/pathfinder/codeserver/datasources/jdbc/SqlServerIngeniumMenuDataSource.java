/*******************************************************************************
 * All right, title and interest in and to the software (the "Software") and the 
 * accompanying documentation or materials (the "Documentation"),
 * including all proprietary rights, therein including all patent rights,
 * trade secrets, trademarks and copyrights, shall remain the exclusive        
 * property of Pablo A. Carbajal Siller.
 * No interest, license or any right respecting the Software and the 
 * Documentation is granted by implication or otherwise.                
 * Dissemination of this information or reproduction of this material is
 * strictly forbidden unless prior written permission is obtained from 
 * the Pablo A. Carbajal Siller.
 *                                                               
 * (C) Copyright 2017 Pablo A. Carbajal Siller
 * All rights reserved. No warranty, explicit or implicit, provided.
 *******************************************************************************/
package com.xsolcorp.pathfinder.codeserver.datasources.jdbc;

import com.solcorp.ingenium.codeserver.datasources.jdbc.IngeniumMenuDataSource;
import com.solcorp.pathfinder.util.logging.Logger;

/**
 * This class resolves the problem that SQL Server doesn't have a native RPAD
 * function, therefore any SQL statements need to qualified the function with
 * the schema. This class returns the same query as its parent however using
 * "%schema%.RPAD(...)" instead of just "RPAD(...)"
 */
public class SqlServerIngeniumMenuDataSource extends IngeniumMenuDataSource {

    private static final Logger LOG = Logger.getLogger(SqlServerIngeniumMenuDataSource.class);
    private static final String MENU_QUERY = "SELECT '00001' AS MENU_GR_CD, COALESCE(MGRP.ETBL_DESC_TXT, '00001') AS MENU_GROUP_NAME, BPF_ID, COALESCE(BPFID.ETBL_DESC_TXT,BPF_ID) AS BPF_NAME, 1 AS MENU_SEQ_NUM FROM %schema%.VBPFU BPFU LEFT OUTER JOIN %schema%.TXTAB MGRP ON  MGRP.ETBL_TYP_ID = 'MGRP' AND MGRP.ETBL_VALU_ID = '00001' AND MGRP.ETBL_LANG_CD = ? LEFT OUTER JOIN %schema%.TXTAB BPFID ON BPFID.ETBL_TYP_ID = 'BPFID' AND BPFID.ETBL_VALU_ID = BPFU.BPF_ID AND BPFID.ETBL_LANG_CD = MGRP.ETBL_LANG_CD WHERE BPFU.CO_ID = ? AND SECUR_CLAS_ID = %schema%.RPAD(CAST(? AS VARCHAR(10)),10) UNION ALL SELECT MENU_GR_CD, COALESCE(MGRP.ETBL_DESC_TXT, MENU_GR_CD) AS MENU_GROUP_NAME, BPF_ID, COALESCE(BPFID.ETBL_DESC_TXT,BPF_ID) AS BPF_NAME, MENU_SEQ_NUM FROM %schema%.VMENV MENV LEFT OUTER JOIN %schema%.TXTAB MGRP ON MGRP.ETBL_TYP_ID = 'MGRP' AND MGRP.ETBL_VALU_ID = MENV.MENU_GR_CD AND MGRP.ETBL_LANG_CD = ? LEFT OUTER JOIN %schema%.TXTAB BPFID ON BPFID.ETBL_TYP_ID = 'BPFID' AND BPFID.ETBL_VALU_ID = MENV.BPF_ID AND BPFID.ETBL_LANG_CD = MGRP.ETBL_LANG_CD WHERE  MENV.CO_ID = ? AND SECUR_CLAS_ID = %schema%.RPAD(CAST(? AS VARCHAR(10)),10) ORDER BY MENU_GR_CD, MENU_SEQ_NUM";

    protected String getQuery() {
	LOG.info("XSOLCORP: returning query from SqlServerIngeniumMenuDataSource.");
	return MENU_QUERY;
    }
}
