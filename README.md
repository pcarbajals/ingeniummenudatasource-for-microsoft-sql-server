# README #

### What is this repository for? ###

* This project resolves the problem when INGENIUM PathFinder makes use of the function RPAD but Microsoft SQL Server doesn't support it natively.
* This project is for demonstration purposes only, therefore no warranty nor set up instructions are provided.
* If you have any question related to INGENIUM PathFinder development, please contact me.
* See legal note below.

### The legal stuff ###

All right, title and interest in and to the software (the "Software") and the 
accompanying documentation or materials (the "Documentation"),
including all proprietary rights, therein including all patent rights,
trade secrets, trademarks and copyrights, shall remain the exclusive        
property of Pablo A. Carbajal Siller.
No interest, license or any right respecting the Software and the 
Documentation is granted by implication or otherwise.                
Dissemination of this information or reproduction of this material is
strictly forbidden unless prior written permission is obtained from 
the Pablo A. Carbajal Siller.
                                                              
(C) Copyright 2017 Pablo A. Carbajal Siller
All rights reserved. No warranty, explicit or implicit, provided.

